/*
 * Master code for plan community simulations.c
 *
 * Calum Brown
 * calum.brown@ed.ac.uk
 *
 /*==================================================================*/
/* HEADER FILES														*/
/*------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/*------------------------------------------------------------------*/
/*==================================================================*/
/* MERSENNE TWISTER DEFINITIONS										*/
/*------------------------------------------------------------------*/
/* Period parameters 												*/
#define N 624
#define M 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a 				*/
#define UPPER_MASK 0x80000000UL /* most significant w-r bits		*/
#define LOWER_MASK 0x7fffffffUL /* least significant r bits 		*/
/*------------------------------------------------------------------*/
/*==================================================================*/
/* FUNCTION PROTOTYPES												*/
/*------------------------------------------------------------------*/
void populate_arena(); /* Establish and fill list 			*/
void species_list(); /* Call appropriate species list function	*/
void species_list1(); /* Fill species_data structures		*/
void species_list2(); /* Fill species_data structures		*/
void display_arena(); /* Display data on-screen			*/
void display_species(); /* Display species data				*/
void output_arena(); /* Output data						*/
void init_genrand(unsigned long s); /* Initialise MT RNG			*/
void iterations(); /* Determine and carry out no. of events*/
void SAD(); /* Produce species abundance distribution	*/
void sort_array(int a[], int n); /* Sorts an array (descending)	*/
void speciation(); /* Simple speciation function					*/
unsigned long genrand_int32(void); /* Mersenne Twister RNG			*/
double genrand_real1(void); /* RNG on [0,1] - calls _int32	*/
int birth_rates(int s); /* Determine individual birth rates	*/
int death_rates(int s); /* Determine individual death rates	*/
float event(); /* All details of next event		*/
float event_time(int g); /* Determine time of next event		*/
int derive_gamma(); /* Total birth and death probabilities	*/
void birth(void *ptr); /* Carry out birth event			 	*/
void death(void *ptr); /* Carry out death event			 	*/
void species_death(int species); /* Remove entry from sp list	*/
void species_death2(void *sp_ptr);
float norm_dispersal(); /* Returns ~N rands						*/
int dens_impact(float x1, float y1, float x2, float y2, int type);
void lottery_rates(); /* Set death rate per species when called	*/
void initialise_dens(); /* Set rates given initial density			*/
void density_dependence(void *ptr, int type);/* Recalculate 'd' after*/
void density_dependence_death(void *ptr); /*	birth or death		*/
void lattice(void *ptr); /* Establish positions in lattice		*/
void species_log(float time, int type); /* Log species nos 		*/
void pcf_by_species(); /* pcf for each species pair				*/
void set_env_points(); /* Set only enc clump centres (randomly)	*/
void set_env_lattice(); /* Find values of environment on lattice	*/
void niche_control(); /* Set niches and tolerances for all species*/
void initialise_environmental_effect();/* For gradient			*/
void environmental_effect(void *ptr);/* Environment for birth		*/
float minimum(float a, float b); /* Returns minimum of a and b		*/
int int_convert(float x); /* Converts float to integer				*/
int round_down(float x); /* Rounds a float down to nearest int		*/
int number_of_species(); /* Returns the current number of species	*/
/*------------------------------------------------------------------*/
/*==================================================================*/
/* STRUCTURES														*/
/*------------------------------------------------------------------*/
/* Set up a structure containing values for species, individual,
 * coordinates, birth and death probabilities, x and y lattice
 * locations, and 2 pointers to structures of the same type.
 *
 * The 'data' structure in fact contains only 1 individual, but
 * there are as many structures of this kind as necessary, created by
 * assigning new memory via pointers to these structures			*/
struct data {
	int species, individual, lat_x, lat_y;
	float xcoord, ycoord, environment, dbase;
	int b,d;
	struct data *prev, *next; /* Pointers to 'data' structures		*/
};
typedef struct data *PTR; /* Can now define a pointer to a data
 *							structure just by entering 'PTR'		*/
/*------------------------------------------------------------------*/
/* Structure containing data for species rather than individuals;
 * id number, and pointers to species' first and last entry in data
 * structure														*/
struct species_data {
	int id, no_inds;
	int env_response; /* Use to denote whether niche has been set	*/
	float env_niche, env_tolerance; /* Environmental impact
	 (to be defined) that species
	 will withstand					*/
	/* Pointers to first and last entry data structures				*/
	PTR first_entry, last_entry;
	/* Pointer to next species_data structure						*/
	struct species_data *prev, *next;
};
typedef struct species_data *SP_PTR; /* For establishing pointers
 to species_data structures		*/
/*------------------------------------------------------------------*/
/* Structure containing environmental point information				*/
struct environment_data {
	int category; /*Only 1 category of environmental points at first*/
	float x, y; /* Location of environmental point					*/
	float value; /* Value of environment at point					*/
	/* Pointer to next data structure								*/
	struct environment_data *prev, *next;
};
typedef struct environment_data *ENV_PTR;
/*------------------------------------------------------------------*/
/*==================================================================*/
/* POINTERS															*/
/*------------------------------------------------------------------*/
PTR head = NULL; /* Head pointer to mark first data entry	*/
SP_PTR sp_head = NULL; /* Head pointer for first species entry		*/
ENV_PTR env_head = NULL;/* Head pointer for environment points list	*/
FILE *fp; /* File pointer								*/
/*------------------------------------------------------------------*/
/*==================================================================*/
/* GLOBAL VARIABLES													*/
/*------------------------------------------------------------------*/
unsigned long seed; /* Seed for RNG						*/
static unsigned long mt[N]; /* the array for the MT state vector*/
static int mti = N+1; /* mti==N+1 means mt[N] is not initialized */
//float global_b = 0.4, global_d = 0.2, global_d_env = 0.4; /* Global birth and death parameters	*/
int global_b = 40000,global_d = 20000, global_d_env = 40000;
float sigma_b = 0.02, sigma_d = 0.01;
//float global_gamma; /* For updating gamma rather than calculating each time */
int global_gamma;
float env_array[100][100]; /* Global array to store environment	*/
/*------------------------------------------------------------------*/
/*==================================================================*/
/* FLAGS															*/
/*------------------------------------------------------------------*/
int output_flag = 1; /* Output to screen on/off						*/
int display_flag = 0; /* Display arena on/off						*/
int SAD_flag = 0; /* Display SAD on/off								*/

int speciation_flag = 1; /* Simple speciation on/off				*/
int dispersal_flag = 1; /* Dispersal limitation on/off				*/
int density_flag = 1; /* Density dependence							*/

int lottery_flag = 1; /* Lottery model (changing b/d rates)			*/
int heteromyopia_flag = 0; /* Species-specific density reactions	*/
int environment_flag = 0; /* Turn on/off niche						*/
int J_C_flag = 0; /* Janzen-Connell model							*/
/*------------------------------------------------------------------*/
/*==================================================================*/
/*==================================================================*/
/* MAIN PROGRAM														*/
/*------------------------------------------------------------------*/
int main(int argc, char *argv[]) {
	printf("\nHello");

	/*	printf("\nPlease enter seed for random number generator: ");
	 scanf("%u", &seed);
	 */

	seed = atoi(argv[1]);
	init_genrand(seed);

	if (environment_flag == 1) {
		set_env_points();
	}

	populate_arena();

	iterations();

	output_arena();

	species_list();

	if (SAD_flag == 1) {
		SAD();
	}
	pcf_by_species();

	return (0);
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* POPULATE ARENA													*/
/* Fill list structure with data									*/
/*------------------------------------------------------------------*/
void populate_arena() {
	/* Pointers to the current, previous and next data structures	*/
	/* TODO: 'Next' pointer isn't used (?)							*/
	PTR ptr = NULL, prev = NULL, next = NULL;
	/* Species, individual, and total individual in each species
	 * variables													*/
	int sp, i, no_i = 100, no_sp = 50, total_inds;
//	float b, d;
	int b,d;

	fp = fopen("time_data.dat", "wb");

	/* Establish a head pointer - a first data structure			*/
	head = (PTR) malloc(sizeof(struct data));
	//	ptr = (PTR) malloc(sizeof(struct data));
	//	prev = (PTR) malloc(sizeof(struct data));
	//	next = (PTR) malloc(sizeof(struct data));

	/* Assign current pointer to the address of the first structure	*/
	ptr = head;
	prev = (PTR) 0; /* 'prev' is a null pointer, '(PTR)' is a cast	*/

	/*	printf("\nPlease enter number of species to begin simulation with: ");
	 scanf("%i", &no_sp);

	 printf("\nPlease enter initial number of individuals per species: ");
	 scanf("%i", &no_i);
	 */
	fprintf(fp, "%i\n", no_i * no_sp);
	fclose(fp);

	/*	printf("\nPlease enter global birth parameter: ");
	 scanf("%f", &global_b);

	 printf("\nPlease enter global death parameter: ");
	 scanf("%f", &global_d);
	 */
	total_inds = no_i * no_sp;

	for (sp = 1; sp <= no_sp; sp++) {
		for (i = 1; i <= no_i; i++) {
			/* Fill values											*/
			ptr->xcoord = 0.0 + genrand_real1();
			ptr->ycoord = 0.0 + genrand_real1();
			ptr->individual = i;
			ptr->species = sp;
			ptr->b = birth_rates(sp);
			ptr->d = death_rates(sp);

			if (lottery_flag == 1) {
				ptr->dbase = ptr->d;
			}
			lattice(ptr); /* Determine position in lattice		*/
			/* Move pointers										*/
			ptr->next = (PTR) malloc(sizeof(struct data));
			ptr->prev = prev;
			prev = ptr;
			ptr = ptr->next;
		}
	}
	/* remove empty data structure at end of list					*/
	ptr = prev;
	ptr->next = (PTR) 0;

	if (lottery_flag == 1) {
		lottery_rates();
	}

	if (density_flag == 1) {
		initialise_dens();
	}

	/* Fill species data structures									*/
	species_list();

	if (environment_flag == 1) {
		initialise_environmental_effect();
	}

	//	if (output_flag == 1)
	//	{
	//		display_arena ();
	//	}
	//	free(ptr);
	species_list();
}/*------------------------------------------------------------------*/
/*==================================================================*/
/* SPECIES LIST														*/
/* Call appropriate species list routine							*/
/*------------------------------------------------------------------*/
void species_list() {
	static int run = 0;

	if (run == 1) {
		species_list2();
	} else {
		species_list1();
		run = 1;
	}
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* SPECIES LIST	1													*/
/* Fill species_data structure for all extant species when called -
 * first run-through only											*/
/*------------------------------------------------------------------*/
void species_list1() {
	PTR iptr = NULL;
	SP_PTR sp_ptr = NULL, sp_prev = NULL, sp_next = NULL;

	sp_head = (SP_PTR) malloc(sizeof(struct species_data));

	iptr = head;
	sp_ptr = sp_head;
	sp_prev = (SP_PTR) 0;

	sp_ptr->id = iptr->species;
	sp_ptr->first_entry = iptr;
	sp_ptr->prev = sp_prev;
	sp_ptr->env_response = 0;

	while (iptr->next != (PTR) 0) {
		iptr = iptr->next;
		if ((iptr->species != sp_ptr->id)) {
			iptr = iptr->prev;
			sp_ptr->last_entry = iptr;
			sp_ptr->no_inds = iptr->individual;
			sp_ptr->next = (SP_PTR) malloc(sizeof(struct species_data));

			sp_ptr->prev = sp_prev;
			sp_prev = sp_ptr;
			sp_ptr = sp_ptr->next;
			iptr = iptr->next;
			sp_ptr->id = iptr->species;
			sp_ptr->first_entry = iptr;
			sp_ptr->env_response = 0;
		}
	}
	if (iptr->next == (PTR) 0) {
		sp_ptr->last_entry = iptr;
		sp_ptr->no_inds = iptr->individual;
		sp_ptr->next = (SP_PTR) 0;
		sp_ptr->prev = sp_prev;
	}

	//	display_species ();

	if (environment_flag == 1) {
		niche_control();
	}
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* SPECIES LIST	2													*/
/* Fill species_data structure for all extant species when called -
 * only when a species list already exists							*/
/*------------------------------------------------------------------*/
void species_list2() {
	int no_sp, species;
	PTR iptr = NULL;
	SP_PTR sp_ptr = NULL, sp_prev = NULL, sp_next = NULL;

	/* Find number of species currently in species list - these will not change	*/
	no_sp = number_of_species();

	iptr = head;
	sp_ptr = sp_head;
	sp_prev = (SP_PTR) 0;
	sp_ptr->prev = sp_prev;

	species = sp_ptr->id;

	/* Until we reach the end of existing species list, just find data from iptr */
	while (sp_ptr->id <= no_sp) {
		species = sp_ptr->id;
		/* Skip any extinct species entries	*/
		if (sp_ptr->no_inds == 0) {
			sp_ptr = sp_ptr->next;
		}
		species = sp_ptr->id;

		/* When species match, copy data	*/
		if (iptr->species == sp_ptr->id) {
			sp_ptr->first_entry = iptr;
			/* Skip to end of species	*/
			while (iptr->species == species && iptr->next != (PTR) 0) {
				iptr = iptr->next;
			}
			if (iptr->next != (PTR) 0 || iptr->species != species) {
				iptr = iptr->prev;
			}
			sp_ptr->no_inds = iptr->individual;
			sp_ptr->last_entry = iptr;
			if (iptr->next != (PTR) 0) {
				iptr = iptr->next;
			}
			/* If at end of individual list before species list, exit and leave
			 * the rest alone - there are no additions or changes to make		*/
			if (iptr->next == (PTR) 0 || iptr->species > no_sp) {
				break;
			}
		}
		if (sp_ptr->next != (SP_PTR) 0) {
			sp_ptr = sp_ptr->next;
		} else if (sp_ptr->next == (SP_PTR) 0) {
			break;
		}
	}
	/* If previous loop was terminated before reaching the end of the species entries,
	 * move to end of species list to prevent overwriting							*/
	if (sp_ptr->id < no_sp) {
		while (sp_ptr->id < no_sp) {
			sp_ptr = sp_ptr->next;
		}
	}

	/* Now we're either at end of species list with more individuals to go,
	 * or we're at the end of both, with no more of either left - in this case
	 * need to exit															 */
	if (iptr->next == (PTR) 0 && iptr->species <= no_sp) {
		printf("\n There are no new species");
	} else if (iptr->next != (PTR) 0 || iptr->species > no_sp) {
		if (iptr->species > no_sp) {
			iptr = iptr->prev;
		}
		while (iptr->next != (PTR) 0) {
			iptr = iptr->next;
			species = iptr->species;
//			printf("  %i", species);
			sp_prev = sp_ptr;

			/* Create new species list entry for iptr species	*/
			sp_ptr->next = (SP_PTR) malloc(sizeof(struct species_data));
			sp_ptr = sp_ptr->next;
			sp_ptr->prev = sp_prev;

			/* Set species and first entry	*/
			sp_ptr->id = species;
			sp_ptr->first_entry = iptr;
			sp_ptr->env_response = 0;

			/* Move iptr onto next species, or to end of list if first	*/
			while (iptr->species == species && iptr->next != (PTR) 0) {
				iptr = iptr->next;
			}
			/* If this isn't the end of the list, have to move back one	*/
			if (iptr->next != (PTR) 0) {
				iptr = iptr->prev;
			}
			/* Set last entry and number of individuals	*/
			sp_ptr->no_inds = iptr->individual;
			sp_ptr->last_entry = iptr;
		}
		/* At this point, have reached the end of the individual list - i.e. there
		 * are no more entries to be made to the species list - need to finish it */
		sp_ptr->next = (SP_PTR) 0;
	}
	/* Now display the species and set niches for any new species	*/
	//	display_species ();

	if (environment_flag == 1) {
		niche_control();
	}
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* DISPLAY ARENA													*/
/* Display data on-screen											*/
/*------------------------------------------------------------------*/
void display_arena() {
	PTR ptr = NULL; /* Current pointer										*/
	ptr = head; /* Start at first data structure					*/

	while (ptr != (PTR) 0) {
		printf("\n%i,%i,%1.4f,%1.4f, %1.4f, %1.4f, %i, %i", ptr->species,
				ptr->individual, ptr->xcoord, ptr->ycoord, ptr->b, ptr->d,
				ptr->lat_x, ptr->lat_y);
		ptr = ptr->next;
	}
	printf("\n");
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* DISPLAY SPECIES													*/
/* Display species data on-screen									*/
/*------------------------------------------------------------------*/
void display_species() {
	SP_PTR sp_ptr = NULL;
	sp_ptr = sp_head; /* Start at first data structure			*/

	printf("\nSpecies Data:\n-------------\n");

	while (sp_ptr != (SP_PTR) 0) {
		printf("%i,%i\n", sp_ptr->id, sp_ptr->no_inds);
		sp_ptr = sp_ptr->next;
	}
	printf("\n");
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* OUTPUT ARENA														*/
/* Write data to file												*/
/*------------------------------------------------------------------*/
void output_arena() {
	PTR ptr = NULL;

	fp = fopen("output.txt", "w");
	fprintf(fp, "x,y,sp \n");

	ptr = head;
	while (ptr != (PTR) 0) {
		fprintf(fp, "%1.4f,%1.4f, %i \n", ptr->xcoord, ptr->ycoord,
				ptr->species);
		ptr = ptr->next;
	}
	fclose(fp);
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* MERSENNE TWISTER 												*/
/* initializes mt[N] with a seed 									*/
/*------------------------------------------------------------------*/
void init_genrand(unsigned long s) {
	mt[0] = s & 0xffffffffUL;
	for (mti = 1; mti < N; mti++) {
		mt[mti] = (1812433253UL * (mt[mti - 1] ^ (mt[mti - 1] >> 30)) + mti);
		mt[mti] &= 0xffffffffUL;
	}
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* MERSENNE TWISTER 												*/
/* generates a random number on [0,0xffffffff]-interval 			*/
/*------------------------------------------------------------------*/
unsigned long genrand_int32(void) {
	unsigned long y;
	static unsigned long mag01[2] = { 0x0UL, MATRIX_A};
	/* mag01[x] = x * MATRIX_A  for x=0,1 */

	if (mti >= N) { /* generate N words at one time */
		int kk;

		if (mti == N + 1) /* if init_genrand() has not been called, */
			init_genrand(5489UL); /* a default initial seed is used */

		for (kk = 0; kk < N-M; kk++) {
			y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
			mt[kk] = mt[kk + M] ^ (y >> 1) ^ mag01[y & 0x1UL];
		}
		for (; kk < N-1; kk++) {
			y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
			mt[kk] = mt[kk + (M-N)] ^ (y >> 1) ^ mag01[y & 0x1UL];
		}
		y = (mt[N-1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
		mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1UL];

		mti = 0;
	}

	y = mt[mti++];

	/* Tempering */
	y ^= (y >> 11);
	y ^= (y << 7) & 0x9d2c5680UL;
	y ^= (y << 15) & 0xefc60000UL;
	y ^= (y >> 18);

	return y;
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* MERSENNE TWISTER 												*/
/* generates a random number on [0,1]-real-interval; calls _int32()	*/
/*------------------------------------------------------------------*/
double genrand_real1(void) {
	return genrand_int32() * (1.0 / 4294967295.0);
	/* divided by 2^32-1 */
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 	BIRTH RATES														*/
/* Determines birth rate of each individual							*/
/*------------------------------------------------------------------*/
int birth_rates(int s) {
	int b = global_b;

	return (b);
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 	DEATH RATES														*/
/* Determines death rate of each individual							*/
/*------------------------------------------------------------------*/
int death_rates(int s) {
	int d;
	if (environment_flag==0)
	{
		d = global_d;
	}
	if (environment_flag==1)
	{
		d = global_d_env;
	}

	return (d);
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 	EVENT															*/
/* Determines all details of the next event to occur				*/
/* by calling other relevant subroutines							*/
/*------------------------------------------------------------------*/
float event() {
	PTR ptr = NULL; /* Current pointer to data structure					*/
	float prob = 0.0, time, x_event = 0.0;
	int gam;
	double randx;
	/* gam = gamma; total birth and death probabilities for scaling
	 * prob = additive probability to match randomly generated one
	 * time = time at which event occurs
	 * randx = random number 0<=x<=1, to be scaled to gamma
	 * x_event = scaled random number; identifies subject of event	*/
	int skip_flag = 0; /* For skipping calculations if speciation occurs */
	static float total_time = 0.0; /* Total time counter			*/
	static int first = 0; /* For doing things first time only		*/
	static int initial_no = 0; /* Initial number of individuals		*/
	static int total_inds = 0; /* Ongoing total number of individuals	*/

	if (first == 0) {
		fp = fopen("time_data.dat", "rb");
		fscanf(fp, "%i", &initial_no);
		fclose(fp);
		remove("time_data.dat");
		total_inds = initial_no;
		first = 1;
		fp = fopen("time_data.dat", "wb");
		fprintf(fp, "Time, Number\n");
		fclose(fp);
		global_gamma = derive_gamma();
	}

/*	fp = fopen("time_data.dat", "a+b");
	if (fp == NULL) {
		printf("\nError: time_data file not found!");
	}
	fprintf(fp, "%10.5f, %i\n", total_time, total_inds);
	fclose(fp);
*/
	/* Generate random number to determine event nature				*/
	randx = genrand_real1();
	/* Find gamma (should be possible to pass it instead?)			*/
	gam = global_gamma;
	/* Determine time of next event							*/
	time = event_time(gam);
	total_time = total_time + time;
	/* If rand_x within speciation probability (1/500), speciation occurs			*/
	if (randx>0.998) {
		speciation();
		total_inds = total_inds + 1;
		species_log(total_time, 1);
		skip_flag = 1;
	}
	/* If speciation has occurred, skip all this	*/
	if (skip_flag==0) {
		/* Then scale 'x' to gamma										*/
		x_event = (randx * gam);
		/* Begin at start of list and find which event occurs to whom	*/
		ptr = head;
		prob = prob + ptr->b;
		prob = prob + ptr->d;
		/* Sum through until position of given event in probability space is found	*/
		while (prob < x_event) {
			ptr = ptr->next;

			if (ptr != (PTR) 0) {
				prob = prob + ptr->b;
				prob = prob + ptr->d;
			}
			if (prob < x_event && ptr == (PTR) 0) {
				printf("\n ERROR - have reached end of list!");
				printf("\n Random no. = %f \n gamma = %f; x_event = %f, prob = %f", randx,gam,x_event,prob);
				prob = x_event; // Made this change Jan 11 to ensure floating point errors can't crash program
				break;
			}
		}

		/* Else test for birth or death of current individual				*/
		prob = prob - ptr->d;
		if (prob < x_event) {
			death(ptr);
			total_inds = total_inds - 1;
			species_log(total_time, 0);
		} else {
			birth(ptr);
			total_inds = total_inds + 1;
		}
	}
}

/*------------------------------------------------------------------*/
/*==================================================================*/
/* 	EVENT TIME														*/
/* Determines 														*/
/*------------------------------------------------------------------*/
float event_time(int g) {
	float time, x;
	int gamma;
	float scaled_gamma;
	/* Total birth and death probability; time variable; random no.	*/
	gamma = g;
	scaled_gamma = gamma/100000.0;
	/* Generate random number & assign to 'x' variable				*/
	x = genrand_real1();
	/* Derive time of next event from this and gamma				*/
	time = ((-log(1.0 - x)) / gamma);
	return (time);
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 	DERIVE GAMMA													*/
/* Determines and returns the value of gamma in the exponential		*/
/* distribution, where gamma is the sum of all individual birth and	*/
/* death rates														*/
/* ADDED: and speciation probability per unit time.
 /*------------------------------------------------------------------*/
int derive_gamma() {
	PTR ptr = NULL;
	int gamma = 0;
	int speciation_probability = 0;

	ptr = head;
	while (ptr != (PTR) 0) {
		gamma = gamma + ptr->b;
		gamma = gamma + ptr->d;
		ptr = ptr->next;
	}
	if (speciation_flag == 1) {
		gamma = gamma + speciation_probability;
	}
	return (gamma);
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 	BIRTH															*/
/* Carries out a birth event on selected individual					*/
/*------------------------------------------------------------------*/
void birth(void *ptr) {
	int species;
	/* pointers to use in creating new entry 						*/
	PTR current = NULL, new_rec = NULL, next = NULL, shifter = NULL;
	int count = 0;
	float death;
	float dist, theta; /* For calculating dispersal	*/
	float rand_disp; /* To determine whether random dispersal occurs,
	 1/100 prob		*/

	rand_disp = genrand_real1() * 100;
	if (rand_disp >= 99) {
		count = 1;
	}

	current = ptr;
	species = current->species;

	if (lottery_flag==1)
	{
		death = current->dbase;
	}

	new_rec = (PTR) malloc(sizeof(struct data));

	/*	Check whether individual is at end of list					*/
	if (current->next == (PTR) 0) {
		new_rec->next = current->next;
		new_rec->prev = current;
		current->next = new_rec;
	} else {
		next = current->next;
		new_rec->next = next;
		new_rec->prev = current;
		current->next = new_rec;
		next->prev = new_rec;
	}

	/* Fill new record */
	if (dispersal_flag == 0 || count == 1) {
		new_rec->xcoord = 0.0 + genrand_real1();
		new_rec->ycoord = 0.0 + genrand_real1();
	}

	if (dispersal_flag == 1 && count == 0) {
		dist = norm_dispersal();
		theta = genrand_real1()*360.0;

		new_rec->xcoord = current->xcoord + (dist*sin(theta));
		new_rec->ycoord = current->ycoord + (dist*cos(theta));

		if (new_rec->xcoord > 1.0) {
			new_rec->xcoord = new_rec->xcoord - 1;
		}
		if (new_rec->xcoord < 0.0) {
			new_rec->xcoord = new_rec->xcoord + 1;
		}

		if (new_rec->ycoord > 1.0) {
			new_rec->ycoord = new_rec->ycoord - 1;
		}
		if (new_rec->ycoord < 0.0) {
			new_rec->ycoord = new_rec->ycoord + 1;
		}
	}

	new_rec->species = species;
	new_rec->individual = (current->individual) + 1;

	new_rec->b = birth_rates(species);
	new_rec->d = death_rates(species);

//	printf("\n New_rec->death rate 1st = %f", new_rec->d);

	if (lottery_flag == 1) {
		new_rec->d = death;
		new_rec->dbase = death;
	}

	/* Add new individual to gamma	*/
	global_gamma = global_gamma + new_rec->b;
	global_gamma = global_gamma + new_rec->d;

	lattice(new_rec);

	if (new_rec->next != (PTR) 0) {
		shifter = new_rec->next;
		while (shifter->species == species) {
			shifter->individual = (shifter->individual) + 1;
			if (shifter->next != (PTR) 0) {
				shifter = shifter->next;
			} else {
				break;
			}
		}

	}

	if (density_flag == 1) {
		density_dependence(new_rec, 1);
	}

//	printf("\n New_rec->death rate 2nd = %f", new_rec->d);

	if (environment_flag == 1) {
		environmental_effect(new_rec);
	}
//	printf("\n New_rec death rate 3rd final = %f", new_rec->d);

}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 	DEATH															*/
/* Carries out a death event on selected individual					*/
/*------------------------------------------------------------------*/
void death(void *ptr) {
	int species, next_species, prev_species;
	/* pointers to use in deleting data entry 						*/
	PTR current = NULL, prev = NULL, next = NULL, shifter = NULL;

	current = ptr;
	species = current->species;

	/* Subtract from global gamma (probability)	 */
	global_gamma = global_gamma - current->b;
	global_gamma = global_gamma - current->d;

	if (density_flag == 1) {
		density_dependence(current, 0);
	}

	next = current->next;
	prev = current->prev;
	if (next == (PTR) 0 || next->species != species) {
		next_species = 0;
	} else {
		next_species = next->species;
	}

	if (prev == (PTR) 0 || prev->species != species) {
		prev_species = 0;
	} else {
		prev_species = prev->species;
	}

	if (current->prev == (PTR) 0) {
		head = next;
		next->prev = prev;
		free(current);
	} else if (current->next == (PTR) 0) {
		free(current);
		prev->next = (PTR) 0;
	} else {
		free(current);
		next->prev = prev;
		prev->next = next;
	}

	shifter = next;

	if (next_species != species && prev_species != species) {
		species_death(species);
	} else if (shifter != (PTR) 0) {
		while (shifter->species == species) {
			shifter->individual = (shifter->individual) - 1;
			if (shifter->next != (PTR) 0) {
				shifter = shifter->next;
			} else {
				break;
			}
		}
	}
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 	SPECIES_DEATH													*/
/* Finds relevant pointer and passes to separate function to
 * delete species entry in list when all members extinct			*/
/*------------------------------------------------------------------*/
void species_death(int species) {
	int sp, no_sp;
	SP_PTR sp_ptr = NULL, sp_prev = NULL, sp_next = NULL;

	sp = species;
	no_sp = number_of_species();

	sp_ptr = sp_head;

	if (sp <= no_sp) {
		while (sp_ptr != (SP_PTR) 0) {
			if (sp_ptr->id == sp) {
				/* Seperate routine to remove relevant species pointer	*/
				species_death2(sp_ptr);
			}
			sp_ptr = sp_ptr->next;
		}
	} else {
		printf("\n Species not found");
	}
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 	SPECIES_DEATH2													*/
/* Receives and deletes species pointer address						*/
/*------------------------------------------------------------------*/
void species_death2(void *sp_ptr) {
	int species_id; /* The number of the extinct species	*/
	SP_PTR current = NULL, prev = NULL, next = NULL;

	current = sp_ptr;

	species_id = current->id;
	current->first_entry = (PTR) 0;
	current->last_entry = (PTR) 0;
	current->no_inds = 0;

	species_log(0, -1);
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 	ITERATIONS														*/
/* Request number of iterations/events and carry them out			*/
/*------------------------------------------------------------------*/
void iterations() {
	int i = 0, it = 14000000, counter = 0, lottery_counter = 0;
	int pcf_counter = 0, gamma_counter = 0;
	float lottery_time;
	static int progress_counter = 0;
	/*	printf("\nHow many iterations? " );
	 scanf("%i", &it);
	 */
	lottery_time = genrand_real1() * 10000.0;

	while (i <= it) {
		event();
		i++;
		counter++;
		pcf_counter++;
		gamma_counter++;

		if (counter == 10000) {
			progress_counter++;
			printf("\n\t--%i iterations--\n", progress_counter * 10000);
			counter = 0;
			//			display_arena();
		}
/*
		if (gamma_counter == 100) {
			printf("\nStored gamma = %i", global_gamma);
			global_gamma = derive_gamma();
			printf("\ntrue gamma = %i", global_gamma);
			gamma_counter = 0;
		}
*/
		/*		if (pcf_counter == 150000)
		 {
		 /*			species_list ();
		 pcf_by_species ();
		 pcf_counter = 0;
		 }
		 */
		if (lottery_flag == 1) {
			lottery_counter++;
			if (lottery_counter > lottery_time) {
				lottery_rates();
				lottery_counter = 0;
				lottery_time = genrand_real1() * 10000.0;
			}
		}
	}
	species_list();
	display_species();
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 	SAD																*/
/* Determine and output species abundance distribution to screen
 * and file.  Also determine number of remaining species and max
 * number of individuals in any one species							*/
/*------------------------------------------------------------------*/
void SAD() {
	SP_PTR sp_ptr = NULL;
	int nmax = 0; /* Maximum number of individuals in any species 	*/
	int n = 0; /* Number of individuals in species				*/
	int counter = 0; /* Count number of remaining species		*/
	fp = fopen("SAD.dat", "wb"); /* Open a writable data file		*/

	sp_ptr = sp_head;

	/* While loop to ensure we're not at the end of the list		*/
	while (sp_ptr != (SP_PTR) 0) {
		fprintf(fp, "%i\n", sp_ptr->no_inds);
		counter = counter + 1;
		/* Test whether this species has more individuals than
		 * previous record-holder, and if so set this new maximum	*/
		n = sp_ptr->no_inds;
		if (n > nmax) {
			nmax = n;
		}
		sp_ptr = sp_ptr->next;
	}
	fclose(fp);
	if (output_flag == 1) {
		printf("\nnmax = %i\nno. of species = %i\n\n", nmax, counter);
	}
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 	SORT_ARRAY														*/
/* 	Sorts any array into descending numerical order					*/
/* (Inefficient. Taken from Kochan, S. 'Programming in C' p.145)	*/
/*------------------------------------------------------------------*/
void sort_array(int a[], int n) {
	int i = 0, j = 0, temp = 0;

	/* Seperate 'for' loops - i.e. loop through all j's for each i	*/
	/* compare values, and if 'i' is less than the next one, move it
	 * forward one place (iteratively)								*/
	for (i = 0; i < n - 1; ++i) {
		for (j = i + 1; j < n; ++j) {
			if (a[i] < a[j]) {
				temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
		}
	}
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 	SPECIATION
 * Introduce 1 individual of new species to end of list				*/
/*------------------------------------------------------------------*/
void speciation() {
	int species;
	int total_number;
	/* pointers to use in creating new entry 						*/
	PTR current = NULL, new_rec = NULL;

	current = head;

	while (current->next != (PTR) 0) {
		current = current->next;
	}
	//		species = current->species;

	total_number = number_of_species();
	species = total_number + 1;

	new_rec = (PTR) malloc(sizeof(struct data));

	new_rec->next = current->next;
	new_rec->prev = current;
	current->next = new_rec;

	/* Fill new record */
	new_rec->xcoord = 0.0 + genrand_real1();
	new_rec->ycoord = 0.0 + genrand_real1();
	lattice(new_rec);
	new_rec->species = species;
	new_rec->individual = 1;
	new_rec->b = birth_rates(new_rec->species);
	new_rec->d = death_rates(new_rec->species);

	if (lottery_flag == 1) {
		new_rec->d = 100000*(genrand_real1() * 0.4);
		new_rec->dbase = new_rec->d;
	}

	/* Gamma	*/
	global_gamma = global_gamma + new_rec->b;
	global_gamma = global_gamma + new_rec->d;

	if (density_flag == 1) {
		density_dependence(new_rec, 1);
	}

	species_list();

	if (environment_flag == 1) {
		niche_control();
		environmental_effect(new_rec);
	}
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 	NORM_DISPERSAL
 * Determine position of offspring in normal dispersal kernel
 * around parent.  To be called when dispersal limitation is in
 * operation, and supplied with parent's position.					*/
/* Code adapted from 'Numerical Recipes in C', p.289				*/
/*------------------------------------------------------------------*/
float norm_dispersal() {
	float rand1 = 0.0, rand2 = 0.0;
	static int number_available = 0;
	static float rand_norm;
	/*static*/
	float sigma = sigma_b;
	float fac, normalise, rsq, r1, r2;

	/* Sigma is in fact the variance, so = sigma^2 in normal notation	*/
	if (sigma == 0.0) {
		printf(
				"\n Please enter a value for s.d. of the normal dispersal kernel:\n");
		scanf("%f", &sigma);
	}

	/* number_available is set to 1 when this function generates
	 * 2 random normal numbers.  It returns one and saves the other
	 * for return when called again.  The flag is then reset to 0	*/
	if (number_available == 0) {
		do {
			rand1 = genrand_real1();
			rand2 = genrand_real1();
			r1 = 2.0 * rand1 - 1.0;
			r2 = 2.0 * rand2 - 1.0;
			rsq = (r1 * r1) + (r2 * r2);
		} while (rsq >= 1.0 || rsq == 0); /* Check they fall within unit circle	*/

		fac = sqrt(-2.0 * log(rsq) / rsq);
		/* Now normalise, using (2/sigma*root(2*pi)) (method in notebook)	*/
		normalise = 1 / (sigma * sigma * 2 * M_PI);
		/* Now do 'Box Muller' transformation to get 2 normal		*/
		/* deviates. Return one and save the other for next time	*/
		rand_norm = ((r1 * fac) * sigma);
		number_available = 1;
		return ((r2 * fac) * sigma);
	} else {
		number_available = 0;
		return rand_norm;
	}
	/* The numbers produced have a mean of 0 and standard deviation	*/
	/* of 1.  Multiply by sigma for different s.d.'s				*/
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 		LOTTERY_RATES
 * Changes the birth and death rates of each species randomly
 * whenever called during simulation - simple lottery model			*/
/*------------------------------------------------------------------*/
void lottery_rates() {
	PTR ptr = NULL;
	int species;
	int death, old_d;

	ptr = head;

	species = ptr->species;

	death = 100000*(genrand_real1() * 0.4);

	printf("\n Lottery rates needs testing; death = %i", death);

	old_d = ptr->dbase;
	ptr->dbase = death;
	ptr->d = ptr->d + (death - old_d);
	ptr = ptr->next;

	while (ptr != (PTR) 0) {
		if (ptr->species == species) {
			ptr->d = ptr->d + (death - old_d);
			ptr->dbase = death;
		} else {
			species = ptr->species;
			death = 100000*(genrand_real1() * 0.4);
			old_d = ptr->dbase;
			ptr->dbase = death;
			ptr->d = ptr->d + (death - old_d);
		}
		ptr = ptr->next;
	}
	global_gamma = derive_gamma();
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 		INITIALISE_DENS
 * Go through newly-established population and set death rates by
 * nearby individuals - density dependence							*/
/*------------------------------------------------------------------*/
void initialise_dens() {
	PTR ptr1 = NULL, ptr2 = NULL; /* For the 2 cycles through list				*/
	float x, y, x2, y2;
	float impact; /* Impact of each individual on death rate,
	 derived from Gaussian distribution			*/
	float focal_species; /* Species of focal individual			*/

	ptr1 = head;

	/* Start ptr1 at beginning of list as focal individual.
	 * then start ptr2 at beginning (every time) and compute
	 * effect of each of the other individuals on the focal			 */
	while (ptr1 != (PTR) 0) {
		focal_species = ptr1->species;
//		ptr1->d = death_rates(focal_species);
		ptr2 = head;

		while (ptr2 != (PTR) 0) {
			if (ptr2 != ptr1) {
				x = ptr1->xcoord;
				y = ptr1->ycoord;
				x2 = ptr2->xcoord;
				y2 = ptr2->ycoord;

				if (ptr2->species == focal_species) {
					impact = dens_impact(x, y, x2, y2, 1);
				} else {
					impact = dens_impact(x, y, x2, y2, 0);
				}

				ptr1->d = ptr1->d + impact;
			}
			ptr2 = ptr2->next;
		}
		ptr1 = ptr1->next;
	}
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 		DENSITY_DEPENDENCE
 * For each birth event, add effect of new individual to all others'
 * death probabilities												*/
/*------------------------------------------------------------------*/
void density_dependence(void *ptr, int type) {
	PTR focal = NULL; /* New individual								*/
	PTR pointer = NULL; /* Each of the other individuals in turn		*/

	float x, y, x2, y2;
	float impact;
	/* Extent = 3*sigma_d, giving the number of lattice squares to consider	*/
	float extent;
	float focal_species; /* Species of focal individual			*/
	float death; /* Underlying death term						*/
	static int range; /* number of lattice squares	*/
	int xmax1, xmax2, ymax1, ymax2, xmin1, xmin2, ymin1, ymin2, i;
	static int run = 0; /* Only need to work out extent of effect once	*/

	if (run == 0) {
		extent = 3 * sigma_d;
		/* Multiply by 10 for range test	*/
		extent = 10.0*extent;
		/* Find range of impact in number of lattice squares affected;
		 * if more than 4, set at 5 - equivalent to all squares			*/
		for (i = 1; i <= 4; i++) {
			if (extent > (i - 1)  && extent <= i) {
				range = i;
				break;
			} else {
				range = 5;
			}
		}
		run = 1;
	}

	focal = ptr;
	pointer = head;

	x = focal->xcoord;
	y = focal->ycoord;
	focal_species = focal->species;

	if (lottery_flag==1)
	{
		death = focal->dbase;
	}

	xmax1 = focal->lat_x + range;
	xmax2 = focal->lat_x - 10 + range;
	ymax1 = focal->lat_y + range;
	ymax2 = focal->lat_y - 10 + range;

	xmin1 = focal->lat_x - range;
	xmin2 = focal->lat_x + 10 - range;
	ymin1 = focal->lat_y - range;
	ymin2 = focal->lat_y + 10 - range;

	while (pointer != (PTR) 0) {
		if (pointer != focal) {
			if ((pointer->lat_x <= xmax1 && pointer->lat_x >= xmin1)
					|| pointer->lat_x <= xmax2 || pointer->lat_x >= xmin2) {
				if ((pointer->lat_y <= ymax1 && pointer->lat_y >= ymin1)
						|| pointer->lat_y <= ymax2 || pointer->lat_y >= ymin2) {
					x2 = pointer->xcoord;
					y2 = pointer->ycoord;

					if (pointer->species == focal_species) {
						impact = dens_impact(x, y, x2, y2, 1);
					}
					if (pointer->species != focal_species) {
						impact = dens_impact(x, y, x2, y2, 0);
					}

					if (type == 1) {
						pointer->d = pointer->d + impact;
						focal->d = focal->d + impact;
						/* Add these to gamma	*/
						global_gamma = global_gamma+impact;
						global_gamma = global_gamma+impact;
					}

					if (type == 0) {
						pointer->d = pointer->d - impact;
						global_gamma = global_gamma - impact;

						if (lottery_flag == 0) {
							if (pointer->d < global_d) {
								/* This remains global_d even under environment model	*/
								/* (so that environmental niche benefits are not overwritten	*/
								global_gamma = global_gamma+(global_d - pointer->d);
								pointer->d = global_d;

							}
						}
//						if (lottery_flag == 1) {
//							if (pointer->d < death) {
//								pointer->d = death;
//							}
//						}
					}
				}
			}
		}
		pointer = pointer->next;
	}
}
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 		DENS_IMPACT
 * Calculation function for density impact of a pointer				*/
/*------------------------------------------------------------------*/
int dens_impact(float x1, float y1, float x2, float y2, int type) {
	float x, y; /* Position of focal individual					*/
	float x_i, y_i; /* Position of other individual					*/
	float x_diff, y_diff, r;/* Difference & distance in positions	*/
	int impact; /* Overall impact of individual					*/
	/*static*/
	float sigma = sigma_d; /* s.d. of normal distribution		*/
	float het_sigma; /* Sigma for heteromyopia kernel - conspecifics
	 have effect over larger areas				*/

	if (sigma == 0.0) {
		printf(
				"\n Please enter a value for s.d. of density dependence (default 0.01):\n");
		scanf("%f", &sigma);
	}

	het_sigma = sigma / 5.0; /* For non-specifics	*/

	x = x1;
	y = y1;

	x_i = x2;
	y_i = y2;

	if (x >= x_i) {
		x_diff = x - x_i;
	} else {
		x_diff = x_i - x;
	}
	if (y >= y_i) {
		y_diff = y - y_i;
	} else {
		y_diff = y_i - y;
	}

	/* Take shortest distance across torus		*/
	if (x_diff > 0.5) {
		x_diff = 1 - x_diff;
	}
	if (y_diff > 0.5) {
		y_diff = 1 - y_diff;
	}
	r = sqrt((x_diff * x_diff) + (y_diff * y_diff));

	if (J_C_flag == 0 && heteromyopia_flag == 0) {
		impact = 100000*(1 / (sigma * sigma * 2 * M_PI)) * exp(-(r * r) / (2 * sigma
				* sigma));
	}
	if (heteromyopia_flag == 1 && type == 1) {
		impact = 100000*(1 / (sigma * sigma * 2 * M_PI)) * exp(-(r * r) / (2 * sigma
				* sigma));
	}
	if (heteromyopia_flag == 1 && type == 0) {
		impact = 100000*(1 / (het_sigma * het_sigma * 2 * M_PI)) * exp(-(r * r) / (2
				* het_sigma * het_sigma));
	}
	if (J_C_flag == 1 && type == 1) {
		impact = 100000*(6.0 / (sigma * sigma * 2 * M_PI)) * exp(-(r * r) / (2 * sigma
				* sigma));
	}
	if (J_C_flag == 1 && type == 0) {
		impact = 100000*(1 / (sigma * sigma * 2 * M_PI)) * exp(-(r * r) / (2 * sigma
				* sigma));
	}

	impact = impact/250000;

	return impact;

}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 		ESTABLISH_LATTICE
 * Establish positions of all individuals within a lattice.
 * When called, this places the individual supplied (via pointer)
 * in the correct lattice square, defined by x and y integer
 * coordinates.  Begin with 10 squares in each direction.			*/
/*------------------------------------------------------------------*/
void lattice(void *ptr) {
	PTR focal = NULL; /* Individual being assigned lattice location		*/
	float x, y; /* Existing x and y coords of pointer				*/
	int n;

	/* Assign 'focal' the correct individual	*/
	focal = ptr;

	x = focal->xcoord;
	y = focal->ycoord;

	/* Multiply x and y by 10 for an integer comparison				*/
	x = 10 * x;
	y = 10 * y;

	for (n = 1; n <= 10; n++) {
		if (x >= n - 1 && x < n) {
			focal->lat_x = n;
		}
		if (y >= n - 1 && y < n) {
			focal->lat_y = n;
		}
	}
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 		SPECIES_LOG
 * Write time and type of each species event to data file			*/
/*------------------------------------------------------------------*/
void species_log(float time, int type) {
	static int first = 0;
	int response;
	int no_sp;
/*
	if (first == 1) {
		fp = fopen("species_log.dat", "a+b");
		fprintf(fp, "%f, %i \n", time, type);
		fclose(fp);
	}
*/
	/* Check whether to append to or overwrite existing file	*/
	if (first == 0) {
		/*		printf("\n Overwrite existing events log file? (y=1/n=0): ");
		 scanf("%i", &response);
		 */response = 1;

		if (response == 1) {
			no_sp = number_of_species();
			fp = fopen("species_log.dat", "wb");
			fprintf(fp, "time, number \n");
			fprintf(fp, "%f, %i \n", time, no_sp);
			fclose(fp);
		}
		first = 1;
	}
}
/*==================================================================*/
/* 		PCF_BY_SPECIES
 * Compute xPOD s.d., mingling index and measure of interspecific segregation 	*/
/*------------------------------------------------------------------*/
void pcf_by_species() {
	PTR ptr1 = NULL, ptr2 = NULL; /* For the 2 cycles through list				*/
	SP_PTR sp_ptr = NULL, sp_ptr2 = NULL;
	PTR MISptr1 = NULL, MISptr2 = NULL;
	SP_PTR MISsp_ptr = NULL, MISsp_ptr2 = NULL;
	float x, y, x2, y2;
	float x_diff, y_diff, r;/* Difference & distance in positions	*/
	float cpcf_bins[10]; /* lists of type and non-type numbers of*/
	float cpcf_norm_bins[10];
	float cpcf_500_bins[10];
	float cpcf_500_norm_bins[10];
	float area_tot = 0.0;		/* These areas are never reset - should they be cumulative across all species?	*/
	float area_500_tot = 0.0;
	/* Still need some values for calculating xPOD areas and then their s.d.	*/
	float norm_denom;
	float bin_size; /* Bandwidth per species	*/
	float ming_con_count;	/* conspecific and heterospecific...*/
	float ming_het_count;	/* counts for the mingling index...	*/
	float ming_500_con_count;
	float ming_500_het_count;
	float av_con_count, av_500_con_count, av_het_count, av_500_het_count;	/* Average counts for 1 species */
	float ming_sp, ming_500_sp;	/* Mingling indices for each species	*/
	float xpod_areas[100000]; /* for storing list of areas	*/
	float xpod_500_areas[100000];
	float xpod_mean = 0.0;
	float xpod_sum_difference = 0.0; /* sum of squared differences for s.d. calculation	*/
	float xpod_500_mean = 0.0;
	float xpod_500_sum_difference = 0.0; /* sum of squared differences for s.d. calculation	*/
	float xpod_sd;
	float xpod_500_sd;
	float ming_tot = 0.0; /* running total proportion (summed proportions for each species)		*/
	float ming_500_tot = 0.0;
	float ming_mean = 0.0;	/* Overall averages						*/
	float ming_500_mean = 0.0;
	int xpod_count = 0; /* count number of species pairs	*/
	int xpod_500_count = 0;
	int sp_count = 0;	/* count of species used for averaging	*/
	int sp_500_count = 0;
	int no_bins; /* number of bins per species	*/
	int focal_species; /* Species of focal individual			*/
	int bin, i, j;
	int species1, species2;
	int species_size;
	int sp1_size, sp2_size;
	int total_number = 0.0;

/*	For Measure of Interpsecific Segregation (MIS):		*/
	int n_id; /* nearest neighbour id; 1 = conspecific, 2 = hetero	*/
	int n_id500;
	int no_con; /* number with conspecific nearest neighbours	*/
	int no_con500;
	int no_het; /* number with heterospecific nearest neighbours	*/
	int no_het500;
	int no_insp; /* species abundance	*/
	int no_outsp; /* total others species abundances	*/
	int no_outsp500;
	float no_con_f, no_con500_f, no_het_f, no_het500_f;
	float no_outsp_f, no_outsp500_f;
	float sp_count_f, sp_500_count_f;
		/* For floating point calculations				*/
	float closest_r; /* closest neighbour distance		*/
	float count_ratio; /* no_con to no_hetero			*/
	float MIS_denom;	/* n(j)-(1/n(i))				*/
	float MIS_sum;	/* Running total					*/
	float MIS_500_sum;
	float mean_MIS;
	float mean_MIS500;
/* ---------------------------------------------------	*/

	float rad = 0.0;/* radius for use in normalisation; mid-box kernel	*/

	/*--------------------------------------------------------------*/
	/* CROSS PAIR CORRELATION FUNCTION, MINGLING INDEX AND M.I.S.	*/
	/*--------------------------------------------------------------*/

	printf("\n pcf by species...");

	fp = fopen("cpcf1.dat", "wb");
	/* Use first species pointer to go through each species once and
	 * second species pointer to go through every other for each
	 * of the first */

	ming_tot = 0.0;
	ming_500_tot = 0.0;

	sp_ptr = sp_head;
	while (sp_ptr != (SP_PTR) 0) {
		species1 = sp_ptr->id;
		sp1_size = sp_ptr->no_inds;

		if (sp1_size > 100) { /* Still need seperate results for abundance threshold of 500	*/

			printf("\n cpcf species %i, size %i\n", species1, sp1_size);
			
			/* These counts are added across the species, then normalised at the species level	*/
			ming_het_count = 0;
			ming_con_count = 0;
			ming_500_het_count = 0;
			ming_500_con_count = 0;

			av_con_count = 0.0;
			av_500_con_count = 0.0;
			av_het_count = 0.0;
			av_500_het_count = 0.0;

			ming_sp = 0.0;
			ming_500_sp = 0.0;

			sp_count++;

//			printf("\n ming_tot, ming_500_tot, sp_count = %f, %f, %i", ming_tot, ming_500_tot, sp_count);

			if (sp1_size > 500 ) {
				sp_500_count++;
			}

			sp_ptr2 = sp_head;
			while (sp_ptr2 != (SP_PTR) 0) {
				species2 = sp_ptr2->id;
				sp2_size = sp_ptr2->no_inds;

				if (sp2_size > 100) {

//					printf("                  %i\n, size %i", species2, sp2_size);

					xpod_count++;
					if (sp1_size>500 && sp2_size > 500) {
						xpod_500_count++;
					}

					no_bins = 10;
					bin_size = 0.25 / no_bins;
					//			printf("\nspecies_size = %i, bin_size = %f, no_bins = %i", species2_size, bin_size, no_bins);

					/* Have removed condition that sp1 and sp2 are different - inserted later	*/
						/* Initialise array at 0 for each species pair	*/
						for (i = 0; i <= 10; i++) {
							cpcf_bins[i] = 0;
							cpcf_500_bins[i] = 0;
						}
						area_tot = 0.0;
						area_500_tot = 0.0;
						/* Use individual pointers to cycle through pairs of
						 * individuals from the 2 species		*/
						ptr1 = head;
						while (ptr1 != (PTR) 0) {
							if (ptr1->species == species1) {
								x = ptr1->xcoord;
								y = ptr1->ycoord;

								ptr2 = head;
								while (ptr2 != (PTR) 0) {
									if (ptr2->species == species2) {
										x2 = ptr2->xcoord;
										y2 = ptr2->ycoord;

										if (x >= x2) {
											x_diff = x - x2;
										} else {
											x_diff = x2 - x;
										}
										if (y >= y2) {
											y_diff = y - y2;
										} else {
											y_diff = y2 - y;
										}

										/* Take shortest distance across torus		*/
										if (x_diff > 0.5) {
											x_diff = 1 - x_diff;
										}
										if (y_diff > 0.5) {
											y_diff = 1 - y_diff;
										}
										r = sqrt((x_diff * x_diff) + (y_diff
												* y_diff));

										/* If distance is within pcf calculation limit, proceed	*/
										if (r < 0.25) {
											/* Find relevant bin	*/
											bin = (r / bin_size);
											/* converts to integer, so can be in 0 bin
											 * (and 0.25 goes into (no_bins-1) bin) */
											if (species1 != species2) {
												cpcf_bins[bin] = cpcf_bins[bin] + 1;

												if (sp1_size>500 && sp2_size > 500) {
													cpcf_500_bins[bin] = cpcf_500_bins[bin] + 1;
												}
											}
											if (r < 0.0167) {
												/* count for mingling index	*/
												if (species1 == species2) {
													if (ptr1 != ptr2) {
														ming_con_count = ming_con_count + 1;
														if (sp1_size>500 && sp2_size > 500) {
															ming_500_con_count = ming_500_con_count + 1;
														}
													}
												}
												if (species1 != species2) {
													ming_het_count = ming_het_count + 1;
														if (sp1_size>500 && sp2_size > 500) {
															ming_500_het_count = ming_500_het_count + 1;
														}
												}
											}
										}
									}
									ptr2 = ptr2->next;
								}
							}
							/* For this individual now have counts for mingling -
							but these need to keep being counted until this species is finished	*/

							ptr1 = ptr1->next;
						} 
						/* Now out of individual loop, so have been through every individual of species 1 and every individual of species 2	*/
						/* Can normalise the xPOD data, but the mingling index is per species 1, so should be normalised after all species 2s	*/
						/* ... and should be set to 0 for every new species 1	*/
						/* Loop to normalise & write to file	*/
						if (species1 != species2) {
						for (i = 0; i <= (no_bins - 1); i++) {
							rad = (i * bin_size) + (bin_size / 2);
							cpcf_bins[i] = cpcf_bins[i] / ((2 * M_PI * rad
									* bin_size * sp1_size * sp2_size)
									+ (M_PI*bin_size * bin_size * sp1_size
											* sp2_size));

							if (sp1_size>500 && sp2_size>500) {
								cpcf_500_bins[i] = cpcf_500_bins[i] / ((2 * M_PI * rad
									* bin_size * sp1_size * sp2_size)
									+ (M_PI*bin_size * bin_size * sp1_size
											* sp2_size));
								area_500_tot = area_500_tot+log((cpcf_500_bins[i]+0.0001))*bin_size;
							}

							/* Calculate area under curve at this point and save it only	*/
							area_tot = area_tot+log((cpcf_bins[i]+0.0001))*bin_size;
							/* Then save vector of areas and find s.d. at end (outside loop)	*/

//							printf("\n area_tot = %f", area_tot);

							fprintf(fp,
									"\n %i, %i, %i, %i, %i, %f, %f, %f",
									species1, sp1_size, species2,
									sp2_size, no_bins, bin_size, rad,
									cpcf_bins[i]);
						}
						}
						xpod_areas[xpod_count] = area_tot;
						
						xpod_mean = xpod_mean + area_tot;
						if (sp1_size>500 && sp2_size > 500) {
							xpod_500_areas[xpod_500_count] = area_500_tot;
							xpod_500_mean = xpod_500_mean + area_500_tot;
						}
					}
				sp_ptr2 = sp_ptr2->next;
			}
		/* Before calculations for next species are carried out, find the mean conspecific and heterospecific counts
		for this species, then find the mingling index for this species: */
		if (ming_con_count > 0) {
			av_con_count = ming_con_count/sp1_size;
			av_het_count = ming_het_count/sp1_size;
			ming_sp = av_het_count/(av_het_count+av_con_count);
			/* Now have the mingling index for this species - keep a running sum of this....	*/
			ming_tot = ming_tot + ming_sp;
		}
		if (ming_500_con_count > 0) {
			av_500_con_count = ming_500_con_count/sp1_size;
			av_500_het_count = ming_500_het_count/sp1_size;
			ming_500_sp = av_500_het_count/(av_500_het_count+av_500_con_count);
			ming_500_tot = ming_500_tot + ming_500_sp;
		}
		}
		sp_ptr = sp_ptr->next;
	}
	fclose(fp);

	ming_mean = ming_tot/sp_count;
	ming_500_mean = ming_500_tot/sp_500_count; /* Both will give infinities if there are no species of that size	*/

	xpod_mean = xpod_mean/xpod_count;
	if (xpod_500_count > 0) {
		xpod_500_mean = xpod_500_mean/xpod_500_count;
	}
	else {
		xpod_500_mean = 0.0;
	}

	for (i = 1; i <= xpod_count; i++) {
		xpod_sum_difference = xpod_sum_difference + (xpod_areas[i]-xpod_mean)*(xpod_areas[i]-xpod_mean);
	}
	for (i=1; i <= xpod_500_count; i++) {
		xpod_500_sum_difference = xpod_500_sum_difference + (xpod_500_areas[i]-xpod_500_mean)*(xpod_500_areas[i]-xpod_500_mean);
	}

	xpod_sd = sqrt(xpod_sum_difference/xpod_count);

	if (xpod_500_count > 0) {
		xpod_500_sd = sqrt(xpod_500_sum_difference/xpod_500_count);
	}
	else {
		xpod_500_sd = 0.0;
	}

/* ------------------------------------------------------------------ */
/* ------------------------------------------------------------------ */
/* ------------------------------------------------------------------ */
/*    M.I.S. Loop													  */
/* ------------------------------------------------------------------ */

	MIS_sum = 0.0;
	MIS_500_sum = 0.0;
	sp_count = 0;
	sp_500_count = 0;

	MISsp_ptr = sp_head;
	while (MISsp_ptr != (SP_PTR) 0) {
		species1 = MISsp_ptr->id;
		sp1_size = MISsp_ptr->no_inds;

		no_con = 0;
		no_het = 0;

		if (sp1_size > 100) { 
			printf("\n MIS %i", species1);
			no_outsp = 0;
			no_insp = 0;
			sp_count++;

			MISptr1 = head;
			while (MISptr1 != (PTR) 0) {
			if (MISptr1->species == species1) {
				x = MISptr1->xcoord;
				y = MISptr1->ycoord;

				closest_r = 0.1;
				no_outsp = 0; /* Set to 0 here to prevent it being summed for every individual	*/
				no_outsp500 = 0;

				no_insp++;

				MISsp_ptr2 = sp_head;
				while (MISsp_ptr2 != (SP_PTR) 0) {
					species2 = MISsp_ptr2->id;
					sp2_size = MISsp_ptr2->no_inds;

					if (sp2_size > 100) {
						if (species2 != species1) {
							no_outsp = no_outsp+sp2_size;
							if (sp2_size > 500) {
								no_outsp500 = no_outsp500+sp2_size;
							}
						}
						MISptr2 = head;
						while (MISptr2 != (PTR) 0) {
							if (MISptr2->species == species2) {
								x2 = MISptr2->xcoord;
								y2 = MISptr2->ycoord;

								if (x >= x2) {
								x_diff = x - x2;
								} else {
								x_diff = x2 - x;
								}
								if (y >= y2) {
								y_diff = y - y2;
								} else {
								y_diff = y2 - y;
								}

								/* Take shortest distance across torus		*/
								if (x_diff > 0.5) {
								x_diff = 1 - x_diff;
								}
								if (y_diff > 0.5) {
									y_diff = 1 - y_diff;
								}
								r = sqrt((x_diff * x_diff) + (y_diff
											* y_diff));

								if (r < closest_r && r > 0.000001) {
									closest_r = r;
									n_id = 2;
									if (species2 == species1) {
										n_id = 1;
									}
								}
							}
							MISptr2 = MISptr2->next;
						}
					}
					MISsp_ptr2 = MISsp_ptr2->next; 
				}
				/* Now we know, for this individual, the identity of the nearest neighbour	*/
				if (n_id == 1) {
					no_con++;
					if (sp1_size > 500) {
					no_con500++;
					}
				}
				if (n_id == 2) {
					no_het++;
					if (sp1_size > 500) {
						no_het500++;
					}
				}
				}
			MISptr1 = MISptr1->next;
			}
		}

		/* Now, before going on to next species, take the total number with con and hetero neighbours and find MIS	*/
		/* MIS = log((no_con/no_het)/(no_in-(1/no_out)))	*/
		
		if (no_het > 0) { /* Only do calculation if some heterospecific neighbours found, saves errors	*/

		no_con_f = no_con;
		no_con500_f = no_con500;
		no_het_f = no_het;
		no_het500_f = no_het500;

		no_outsp_f = no_outsp;
		no_outsp500_f = no_outsp500;

		count_ratio = no_con_f/no_het_f;		
		MIS_denom = no_insp-(1.0/no_outsp_f);

		MIS_sum = MIS_sum + log(count_ratio/MIS_denom);
		
		if (sp1_size > 500) {
			sp_500_count++;

			count_ratio = no_con500_f/no_het500_f;
			MIS_denom = no_insp-(1/no_outsp500_f);
			MIS_500_sum = MIS_500_sum + log(count_ratio/MIS_denom);
		}
		}
		MISsp_ptr = MISsp_ptr->next;
	}

	sp_count_f = sp_count;
	sp_500_count_f = sp_500_count;
								
	mean_MIS = MIS_sum/sp_count_f;
	mean_MIS500 = MIS_500_sum/sp_500_count_f;

	fp = fopen("results.dat", "wb");
	fprintf(fp,"\n %f, %f, %f, %f, %f, %f",ming_mean,xpod_sd,mean_MIS,ming_500_mean,xpod_500_sd,mean_MIS500);
	fclose(fp);

}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 		SET_ENV_POINTS
 * Distribute environmental points representing clump centres or
 * gradient summits randomly										*/
/*------------------------------------------------------------------*/
void set_env_points() {
	ENV_PTR env_ptr = NULL, env_prev = NULL, env_next = NULL;
	int no_points = 1000;
	int i;
	int count = 0;
	float xstore, ystore;	/* Store x and y coords of 'parent' point	*/

	env_head = (ENV_PTR) malloc(sizeof(struct environment_data));

	env_ptr = env_head;
	env_prev = (ENV_PTR) 0;

	/* Distribute points randomly						*/
	for (i = 1; i <= no_points; i++) {
		/* When 'parent' point has been established, set 9 others around it	*/
//		if (count != 0)
//		{
/*			env_ptr->category = 2;
			env_ptr->x = xstore + norm_dispersal();
			env_ptr->y = ystore + norm_dispersal();
			if (env_ptr->x > 1.0) {
				env_ptr->x = env_ptr->x - 1;
			}
			if (env_ptr->x < 0.0) {
				env_ptr->x = env_ptr->x + 1;
			}

			if (env_ptr->y > 1.0) {
				env_ptr->y = env_ptr->y - 1;
			}
			if (env_ptr->y < 0.0) {
				env_ptr->y = env_ptr->y + 1;
			}
			env_ptr->value = genrand_real1();
			count++;
*/	//	}
//		if (count == 0)
//		{
			env_ptr->category = 2;
			env_ptr->x = 0.0 + genrand_real1();
			env_ptr->y = 0.0 + genrand_real1();
			env_ptr->value = genrand_real1();
//			xstore = env_ptr->x;
//			ystore = env_ptr->y;
//			count++;
//		}
//		if (count == 10)
//		{
//			count = 0;
//		}

		if (i < no_points) {
			env_ptr->next = (ENV_PTR) malloc(sizeof(struct environment_data));
			env_ptr->prev = env_prev;
			env_prev = env_ptr;
			env_ptr = env_ptr->next;
		} else {
			env_ptr->next = (ENV_PTR) 0;
			env_ptr->prev = env_prev;
		}
	}
	set_env_lattice ();
}
/*------------------------------------------------------------------*/
/*==================================================================*/
/* 		SET_ENV_LATTICE
 * Find value of environment at every point and re-scale to [0,1]	*/
/*------------------------------------------------------------------*/
void set_env_lattice() {
	ENV_PTR env_ptr = NULL;
	float point_value, env_value, new_value;
	float x, y, xenv, yenv, r;
	float xdiff, ydiff;
	int i, j;
	float min = 100000000.0;
	float max = 0.0;
	float range;

	for (i = 0; i <= 99; i++) {
		x = (i + 0.5) / 100;

		xdiff = 50.0-i;

		for (j = 0; j <= 99; j++) {
//			env_ptr = env_head;
//			env_value = 0.0;

			y = (j + 0.5) / 100;

/*			while (env_ptr != (ENV_PTR) 0) {
				xenv = env_ptr->x;
				yenv = env_ptr->y;

				x_diff = fabs(x - xenv);
				y_diff = fabs(y - yenv);

				x_diff = minimum(x_diff, 1 - x_diff);
				y_diff = minimum(y_diff, 1 - y_diff);

				r = sqrt((x_diff * x_diff) + (y_diff * y_diff));
*/
				/* Add effect of this point	*/
/*				point_value = env_ptr->value;
				point_value = point_value - (r * 10);
				if (point_value < 0.0) {
					point_value = 0.0;
				}

				env_value = env_value + point_value;

				env_ptr = env_ptr->next; */

// Environmental value = x-gaussian + y-gaussian; sigma in both = 35 in terms of i,j

				ydiff = 50.0-j;

				r = sqrt((xdiff * xdiff) + (ydiff * ydiff));
// Gaussian:
//				env_value = (1 / (sqrt(2 * M_PI*50*50))) * exp(-(r*r)/(2*50*50));
// Circle:
//				env_value = ((2/(M_PI*60*60))*sqrt((60*60)-(xdiff*xdiff)))+
//				((2/(M_PI*60*60))*sqrt((60*60)-(ydiff*ydiff)));
// Circular quadrants:

			if (xdiff<0.0){
				xdiff = xdiff*-1.0;
			}

			if (ydiff<0.0){
				ydiff = ydiff*-1.0;
			}

			if (xdiff>=ydiff){
				env_value = (2/M_PI*100*100)*sqrt((100*100)-(xdiff*xdiff));
			}
			else {
				env_value = (2/M_PI*100*100)*sqrt((100*100)-(ydiff*ydiff));
			}

			env_array[i][j] = env_value;

			if (env_value > max) {
				max = env_value;
			}
			if (env_value < min) {
				min = env_value;
			}
		}

	}

	/* Now re-scale matrix values to [0,1] interval	*/
	/* and write matrix to file						*/
	fp = fopen("Environment.dat", "wb");

	range = max-min;
	printf("\nMax= %f; Min = %f, range = %f", max, min,range);

	for (i = 0; i <= 99; i++) {
		fprintf(fp, "\n");
		for (j = 0; j <= 99; j++) {
			env_value = env_array[i][j];
			new_value = (env_value - min) / range;
			env_array[i][j] = new_value;

			fprintf(fp, "%f, ", new_value);
		}
	}
	fclose(fp);
}
/*==================================================================*/
/* 		NICHE_CONTROL
 * Give each initial species a niche optimum on [0,1].  Tolerance
 * set for all.														*/
/*------------------------------------------------------------------*/
void niche_control() {
	float tolerance = 0.2; /* Tolerance set for all (= sigma in gaussian)*/
	SP_PTR sp_ptr = NULL, sp_prev = NULL, sp_next = NULL;

	sp_ptr = sp_head;

	while (sp_ptr != (SP_PTR) 0) {
		if (sp_ptr->env_response != 1) {
			sp_ptr->env_niche = genrand_real1();
			sp_ptr->env_tolerance = tolerance;
			sp_ptr->env_response = 1;
		}
		sp_ptr = sp_ptr->next;
	}
}
/*==================================================================*/
/* 		INITIALISE_ENVIRONMENTAL_EFFECT
 * Determine environmental effect from matrix						*/
/*------------------------------------------------------------------*/
void initialise_environmental_effect() {
	PTR ptr = NULL; /* For individuals				*/
	SP_PTR sp_ptr = NULL; /* For finding species responses	*/
	float env_value;
	float x, y;
	int xind, yind;
	float niche, tolerance;
	float diff, gauss_r, resp, env_resp;
	int scaled_resp;
	int max = global_d_env; /* Maximum value for death parameter	*/
	int opt = 0000; /* value of decrease to death term in optimum environment... */
			/*...scale to get average at 0.2								*/
	float opt_extent = 0.00; /* Extent of flat-topped niche optimum			*/

	/* Start ptr at beginning of list then find x and y coords for matrix	 */
	/* (multiply by 100 and round down)	*/
	ptr = head;
	while (ptr != (PTR) 0) {
		x = ptr->xcoord;
		y = ptr->ycoord;
		x = 100.0 * x;
		y = 100.0 * y;
		xind = round_down(x);
		yind = round_down(y);
		ptr->environment = 0;

		/* Find niche and tolerance of individual's species	*/
		sp_ptr = sp_head;
		while (sp_ptr != (SP_PTR) 0) {
			if (sp_ptr->id == ptr->species) {
				niche = sp_ptr->env_niche;
				tolerance = sp_ptr->env_tolerance;
			}
			sp_ptr = sp_ptr->next;
		}

		env_value = env_array[xind][yind];

		/* Now, find the difference between the species' niche mean and this environment */
		diff = (niche - env_value); /* For stronger niche response, give flat top	*/
		/* For flat-top niche response:
		 * at optimum value at +/- opt_extent, then falls away according to tolerance  */
		if (diff>=(opt_extent*-1.0) && diff<=opt_extent) {
			diff = 0.0;
		}
		else {
			if (diff< 0.0){
				diff = diff+opt_extent;
			}
			if (diff> 0.0){
				diff = diff-opt_extent;
			}
		}
		/* ...and re-scale to standard normal (+/- 3 sigma)	*/
		gauss_r = diff * (3.0 / tolerance);

		/* Now find value of response in Gaussian around niche mean	*/
		resp = (1 / (sqrt(2 * M_PI))) * exp(-(gauss_r * gauss_r) / 2);
		/* ...and scale to interval (0,1) for multiplication with existing birth rate	*/
		env_resp = resp / 0.4;
		scaled_resp = env_resp*opt;

		/* Finally change death parameter to reflect environment */
		ptr->d = ptr->d - scaled_resp;

		/* Set as equal to global_d so that maximum environmental benefit is to this point	*/
		if (ptr->d < global_d)
		{
			ptr->d = global_d;
		}

		ptr = ptr->next;
	}
}
/*==================================================================*/
/* 		ENVIRONMENTAL_EFFECT
 * Determine environmental effect 									*/
/*------------------------------------------------------------------*/
void environmental_effect(void *ptr) {
	PTR new_ind = NULL; /* For individuals				*/
	SP_PTR sp_ptr = NULL; /* For finding species responses	*/
	float env_value;
	float x, y;
	int xind, yind;
	float niche, tolerance;
	float diff, gauss_r, resp, env_resp;
	int max = global_d_env;
	int opt = 0000;
	int scaled_resp;
	float opt_extent = 0.00; /* Extent of flat-topped niche optimum			*/

	new_ind = ptr;
	x = new_ind->xcoord;
	y = new_ind->ycoord;
	x = 100.0 * x;
	y = 100.0 * y;
	xind = round_down(x);
	yind = round_down(y);

	/* Find niche and tolerance of individual's species	*/
	sp_ptr = sp_head;
	while (sp_ptr != (SP_PTR) 0) {
		if (sp_ptr->id == new_ind->species) {
			niche = sp_ptr->env_niche;
			tolerance = sp_ptr->env_tolerance;
		}
		sp_ptr = sp_ptr->next;
	}

	env_value = env_array[xind][yind];

	/* Now, find the difference between the species' niche mean and this environment */
	diff = (niche - env_value);
	/* For flat-top niche response:
	 * at optimum value at +/- opt_extent, then falls away according to tolerance  */
	if (diff>=(opt_extent*-1.0) && diff<=opt_extent) {
		diff = 0.0;
	}
	else {
		if (diff< 0.0){
			diff = diff+opt_extent;
		}
		if (diff> 0.0){
			diff = diff-opt_extent;
		}
	}
	/* ...and re-scale to standard normal (+/- 3 sigma)	*/
	gauss_r = diff * (3.0 / tolerance);
	/* Now find value of response in Gaussian around niche mean	*/
	resp = (1 / (sqrt(2 * M_PI))) * exp(-(gauss_r * gauss_r) / 2);
	/* ...and scale to interval (0,1) for multiplication with existing birth rate	*/
	env_resp = resp / 0.4;
	scaled_resp = env_resp*opt;

	new_ind->d = new_ind->d - scaled_resp;
	global_gamma = global_gamma - scaled_resp;

	if (new_ind->d < global_d)
	{
		global_gamma = global_gamma + (global_d - new_ind->d);
		new_ind->d = global_d;
	}
}
/*==================================================================*/
/* 		MINIMUM
 * Return the minimum of 2 values (float in this case)				*/
/*------------------------------------------------------------------*/
float minimum(float a, float b) {
	if (a <= b) {
		return a;
	} else {
		return b;
	}
}
/*==================================================================*/
/* 		INT_CONVERT
 * Convert supplied float number to integer							*/
/*------------------------------------------------------------------*/
int int_convert(float x) {
	int i = 0;

	while (i < x) {
		i++;
	}
	return i;
}
/*==================================================================*/
/* 		ROUND_DOWN
 * Convert supplied float number to integer							*/
/*------------------------------------------------------------------*/
int round_down(float x) {
	int i = 0;

	while (i < x) {
		i++;
	}
	return i - 1;
}
/*==================================================================*/
/* 		NO_SPECIES
 * Return the current number of species in the simulation			*/
/*------------------------------------------------------------------*/
int number_of_species() {
	int total_number = 0;
	SP_PTR sp_ptr = NULL;

	sp_ptr = sp_head;

	while (sp_ptr != (SP_PTR) 0) {
		total_number++;
		sp_ptr = sp_ptr->next;
	}
	return total_number;
}
/*==================================================================*/
/* 						*/
/*------------------------------------------------------------------*/
