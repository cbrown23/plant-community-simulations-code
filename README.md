# README #

Code for running ecological simulations of plant communities under neutral, niche, lottery, Janzen-Connell and heteromyopia coexistence mechanisms (selected through global flags at top of code). For details of the model please see: Brown C, Law R, Illian JB, Burslem DFRP (2011) Linking ecological processes with spatial and non-spatial patterns in plant communities. Journal of Ecology 99, 1402-1414.

Code is ready to be compiled and run. For assistance contact Calum Brown, calum.brown@ed.ac.uk.